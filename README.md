# AWS Exporter

A Prometheus exporter that exposes various AWS metrics , such as estimated costs. 

## Current Supported Metrics

```
# HELP aws_estimated_monthly_costs_total The current estimated monthly cost by cost allocation tag, in USD
# TYPE aws_estimated_monthly_costs_total counter
aws_estimated_monthly_costs_total{tagKey="Account",tagValue="sample"} 12.1796753234

# HELP aws_estimated_monthly_costs_last_update_seconds The timestamp of the last update for estimated costs
# TYPE aws_estimated_monthly_costs_last_update_seconds counter
aws_estimated_monthly_costs_last_update_seconds 1637334730
```

## Configuration

The exporter currently supports only a single tag, but we hope to expand to more as needed in the future.

| Env Var | Description | Required? | Default |
|---------|-------------|-----------|---------|
| `TAG_KEYS` | The cost allocation tag to look for, comma-separated | Yes | - |
| `PORT` | The port to run the HTTP server on | No | 3000 |


## Development

This project provides a `docker-compose.yml` file, letting you spin up the environment using only `docker compose up`. This will mount the local AWS config file into the container and use it.

**NOTE:** This is expecting you are using `saml2aws` to authenticate to AWS, per the instructions documented on the [Configuring Local AWS Auth for VT SAML blog post](https://devcom.vt.edu/posts/2021/08/configuring-local-aws-auth-for-vt-saml/). Since the container doesn't have saml2aws or access to your machine's credentials, you will need to authenticate beforehand.

```bash
export AWS_PROFILE=vt-platform-prod
aws s3 ls # just to auth
docker compose up
```

Once launched, you can view the metrics at [https://app.localhost.devcom.vt.edu/metrics](https://app.localhost.devcom.vt.edu/metrics).