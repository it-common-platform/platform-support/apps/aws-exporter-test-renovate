FROM ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/node:lts-alpine AS base
WORKDIR /usr/local/app

FROM base
COPY package.json yarn.lock ./
RUN yarn install --production && \
    yarn cache clean
COPY src ./src
CMD ["node", "src/index.js"]
